#!/bin/bash

while getopts f: flag
do
    case "${flag}" in
        f) filearg=${OPTARG};;
    esac
    shift $((OPTIND -1))
done
file=${filearg:-.env}

echo "File: $file";

echo "" > $file
i=1;
for envname in "$@"
do
    echo "Env name: $envname - value ${!envname}";
    echo "$envname=${!envname}" >> $file
    i=$((i + 1));
done
