#!/bin/bash
echo "Building"
set -eo pipefail
export AWS_DEFAULT_REGION=eu-west-1
VER=$(cat .version)
[[ "$VER" =~ ^[0-9]+.[0-9]+.[0-9]+$ ]] || (echo Bad version && exit 1)

ECR_NAME=$ECR_REPOSITORY_NAME:$VER

NEEDDOCKER=$([[ -f Dockerfile && ! -v NO_DOCKER ]] && echo true || echo false)
echo Need Docker $NEEDDOCKER

if [[ "$NEEDDOCKER" == true && -z $ECR_REPOSITORY_NAME ]]
then
    echo Missing ECR_REPOSITORY_NAME
    exit 1
fi

if [[ "$NEEDDOCKER" == true && ! -z $AWS_SECRET_ACCESS_KEY && ! -z $AWS_ACCESS_KEY_ID ]]
then
    $(aws ecr get-login --no-include-email)
else
    echo WARNING: Missing AWS access keys
fi

$NEEDDOCKER && docker build --build-arg ssh_key="$(cat /opt/atlassian/pipelines/agent/ssh/id_rsa)" -t $ECR_NAME .

verlte() {
  [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ];
}

createtag() {
  if [[ "$(git tag -l $1)" == "$1" ]]
  then
      git push --delete origin $1
      git tag -d $1
  fi
  git tag -f $1
}

LATEST=$(git tag -l | egrep '^[0-9]+\.[0-9]+\.[0-9]+'$ | tac | head -n1)
echo Latest Version: $LATEST
echo Version: $VER
lte=$($(verlte $VER $LATEST) && echo yes || echo no)
echo Need to deploy: $lte

MINOR=$([[ "$VER" =~ ^[0-9]+.[0-9]+ ]] && echo "${BASH_REMATCH[0]}")
MAJOR=$([[ "$VER" =~ ^[0-9]+ ]] && echo "${BASH_REMATCH[0]}")

if [[ "$lte" == "no" && -z $DOCKER_BUILD_TAG ]]
then
  echo "Preparing to release"

  if [[ "$NEEDDOCKER" == true ]]
  then
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:$MINOR
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:$MAJOR
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:latest

    docker push $ECR_REPOSITORY_NAME:$MINOR
    docker push $ECR_REPOSITORY_NAME:$MAJOR
    docker push $ECR_REPOSITORY_NAME:latest
    docker push $ECR_NAME
  fi

  createtag $MAJOR
  createtag $MINOR
  createtag $VER

  git push --tags
elif [[ ! -z $DOCKER_BUILD_TAG && "$NEEDDOCKER" == true ]]
then
    echo "Preparing to release to custom docker tag $MINOR-$DOCKER_BUILD_TAG"
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:$MINOR-$DOCKER_BUILD_TAG
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:$MAJOR-$DOCKER_BUILD_TAG
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:latest-$DOCKER_BUILD_TAG

    docker push $ECR_REPOSITORY_NAME:$MINOR-$DOCKER_BUILD_TAG
    docker push $ECR_REPOSITORY_NAME:$MAJOR-$DOCKER_BUILD_TAG
    docker push $ECR_REPOSITORY_NAME:latest-$DOCKER_BUILD_TAG
    docker push $ECR_NAME
fi

if [[ "$NEEDDOCKER" == true  && -v ENABLE_COMMIT_BUILDS ]]
then
  echo COMMIT BUILD
  ECR_NAME=$ECR_REPOSITORY_NAME:master-${BITBUCKET_COMMIT:0:8}
  docker push $ECR_NAME
  docker tag $ECR_NAME $ECR_REPOSITORY_NAME:master
  docker push $ECR_REPOSITORY_NAME:master
fi

echo Finished

