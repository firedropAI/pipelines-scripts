#!/bin/bash
set -eo pipefail
export AWS_DEFAULT_REGION=eu-west-1

NEEDDOCKER=$([[ -f Dockerfile && ! -v NO_DOCKER ]] && echo true || echo false)
echo Need Docker $NEEDDOCKER

if [[ "$NEEDDOCKER" == true && -z $ECR_REPOSITORY_NAME ]]
then
    echo Missing ECR_REPOSITORY_NAME
    exit 1
fi

if [[ "$NEEDDOCKER" == true && ! -z $AWS_SECRET_ACCESS_KEY && ! -z $AWS_ACCESS_KEY_ID ]]
then
    $(aws ecr get-login --no-include-email)
else
    echo WARNING: Missing AWS access keys
fi

if [[ $NEEDDOCKER ]]
then
    ECR_NAME=$ECR_REPOSITORY_NAME:$BITBUCKET_BRANCH-${BITBUCKET_COMMIT:0:8}
    docker build --build-arg ssh_key="$(cat /opt/atlassian/pipelines/agent/ssh/id_rsa)" -t $ECR_NAME .
    docker tag $ECR_NAME $ECR_REPOSITORY_NAME:$BITBUCKET_BRANCH

    docker push $ECR_NAME
    docker push $ECR_REPOSITORY_NAME:$BITBUCKET_BRANCH
fi
