#!/bin/bash
echo "Checking Version"
set -eo pipefail
VER=$(cat .version)
[[ "$VER" =~ ^[0-9]+.[0-9]+.[0-9]+$ ]] || exit 1
echo ".version file found. Version: $VER"

verlte() {
  [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ];
}

if git show-ref --tags | egrep -q "refs/tags/$VER$"
then
  echo "The version ($VER) in .version already exists"
  exit 1
else
    echo "All good. Version tag not found"
fi
